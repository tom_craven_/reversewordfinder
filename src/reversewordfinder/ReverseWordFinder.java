/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package reversewordfinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Tom Craven
 */
public class ReverseWordFinder {

    private final HashMap hm;
    private final HashSet results;
    private List<String> list;
    private int count=0;
    
    /**
     *piece of code that reads text from a text file and efficiently finds all
     * the words that are the reverse of other words, and prints these out 
     * as a list sorted by string length with each pair occurring once.
     * 
     * For example ( this is just a very basic one)  
     * the output from a file containing 
     * "what he saw was not part of a trap just a ton of snow" 
     * would output
     * saw, was
     * not, ton
     * part, trap
     */
    public ReverseWordFinder() {
        hm = new HashMap();
        results = new HashSet();
        list = Arrays.asList("default1", "default2", "default3");
    }
   
    private static String reverseString(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        return sb.toString();
    } 

    /**
     *
     * @param INPUTTEXTFILEPATH provide the path of the text file to be analysed
     * system buffers the input returning a stream of words as a list. 
     * A hashmap is used to filter out the double entries and in the case
     * that a word causes a hash clash but is not a double it is resolved.
     *  
     */
    public void streamFileToHashMap(String INPUTTEXTFILEPATH) {

        try (BufferedReader br = Files.newBufferedReader(Paths.get(INPUTTEXTFILEPATH))) {

            list = br.lines()
                    .map(String::toLowerCase)
                    .map(s -> s.split("\\s+")).flatMap(Arrays::stream)
                    .filter(s -> !s.isEmpty())
                    .map(s -> s.trim())
                    .filter(s -> s.length() >= 3)
                    .collect(Collectors.toList());
           
            list.forEach((t) -> {          
                int index = t.hashCode();            
                hm.put(index, t);
            });
        } catch (IOException e) {
            System.err.println("failed to stream!");
        }
    }
    
    /**
     *Iterate through the hash map values and find the reversed words by looking
     * up the hash of the reversed word. containsKey() is a get()
     * that throws away the retrieved value, it's O(1) Complexity is an 
     * improvement over a linear search.  
     * @return list of words which exist in reverse order in this input text
     */
    public List<String> getResult() {
        //clear the list before reuse
        list.clear();  
        Iterator itt = hm.values().iterator();
        while (itt.hasNext()) {
            Object value = itt.next();
            String reverseValue = reverseString((String) value);
            int index = reverseValue.hashCode();           
            if (hm.containsKey(index)) {             
                results.add(value);
                itt.remove();    
            }
        } 
        list.addAll(results);
        Collections.sort(list, (String p1, String p2) -> p1.length() - p2.length());
        list.forEach((o) -> {
            System.out.println(o + ", " + reverseString((String) o));
            count++;
        });     
        return list;    
    }
    public int getWordCount(){
        return hm.size();
    }
}
