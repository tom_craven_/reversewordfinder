/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package reversewordfinder;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tom
 */
public class ReverseWordFinderIT {

    ReverseWordFinder instance;

    /**
     * Test class for Reverse Word Finder
     */
    public ReverseWordFinderIT() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of streamFileToHashMap method, of class ReverseWordFinder.
     */
    @Test
    public void testStreamFileToHashMap() {
        instance = new ReverseWordFinder();
        System.out.println("###--streamFileToHashMap--###");
        String INPUTTEXTFILEPATH = "test//reversewordfinder//largeTextBody.txt";
        instance.streamFileToHashMap(INPUTTEXTFILEPATH);
        System.out.println(instance.getWordCount());
        assertTrue(instance.getWordCount() == 80270);
    }

    /**
     * Test of getResult method, of class ReverseWordFinder using a list of 523
     * reversible words
     */
    @Test
    public void testReverseWords() {
        instance = new ReverseWordFinder();
        System.out.println("###--getResult--###");
        String INPUTTEXTFILEPATH = "test//reversewordfinder//reverseWords.txt";
        instance.streamFileToHashMap(INPUTTEXTFILEPATH);
        List<String> list = instance.getResult();
        assertTrue(list.size() == 523);
    }

    /*
    *Test for false positives
     */
    @Test
    public void testFalsePositive() {
        instance = new ReverseWordFinder();
        System.out.println("###--Test for false positives--###");
        String INPUTTEXTFILEPATH = "test//reversewordfinder//notReverseWords.txt";
        instance.streamFileToHashMap(INPUTTEXTFILEPATH);
        List<String> list = instance.getResult();
        assertTrue(list.isEmpty());
    }

    /*
    *Test palindromes
     */
    @Test
    public void testPalindromes() {
        instance = new ReverseWordFinder();
        System.out.println("###--Test for palindromes--###");
        String INPUTTEXTFILEPATH = "test//reversewordfinder//palindromes.txt";
        instance.streamFileToHashMap(INPUTTEXTFILEPATH);
        List<String> list = instance.getResult();
        int expectedResult = 19;
        int actualResult = list.size();
        System.out.println(actualResult);
        assertTrue(actualResult == expectedResult);
    }

    /*
    *Test with a 80,00 word input text
     */
    @Test
    public void testLargeInputText() {
        instance = new ReverseWordFinder();
        System.out.println("###--Test a large input text--###");
        String INPUTTEXTFILEPATH = "test//reversewordfinder//largeTextBody.txt";
        instance.streamFileToHashMap(INPUTTEXTFILEPATH);
        List<String> list = instance.getResult();
        assertTrue(list.size() == 523);
    }

}
